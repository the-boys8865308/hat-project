# EEE3088F HAT Project


## Authors

- Nang'alelwa Sitwala (stwnan001@myuct.ac.za)
- Longa Maimbo (mmblin001@myuct.ac.za)
- Thatohatsi Motlhamme (mtltha070@myuct.ac.za)

## Information on the repository:

This is the official repository for our EEE3088F HAT Project. Our HAT makes use of a proximty sensor and a battery level detector. The proximity sensor will be used to determine the distance between the sensor and the area the sensor is pointing at while the voltage detector will be used to measure and keep track of the voltage across the battery terminals The repo contains the modules, peripherals and files used to design and operate the HAT. The HAT can be attached to a car's exterior to measure how near it is to another object while reversing or parking; it could be attached to a hat to help visually impaired individuals to have better distance perception and it can also be attached to a security system at the entrance of a home, as to alert a user if the door has been opened.

## Who is the target audience:

The target audience for our project constitutes of motor vehicle owners, individuals who are visually impaired, hobbyists who might seek to add proximity sensing to their own projects or devices and housing security systems.

## What Hardware you will need:

- An STM32FO51C6 microcontroller.
- A PC with a minimum of 4GB ram to ease your work when coding.
- A micro USB cable to insert code into the STM32FO microcontroller.
- An 18650 battery holder and a li-on battery.

## What Software you will need:

- A working STM cube ide on your PC (or any other suitable alternative).
- A working KiCad 7.0 software to access schematic and pcb files (or an other suitable alternative).

## How to connect the hardware up:


- Connect the battery holder on the board and place the li-on battery.	
- Connect the microcontroller on the correct pins on the board.
- Connect the micro USB cable from the PC to the board.

## How to run the equivalent of a ‘hello-world’ program that uses the features of PCB:

To be updated shortly

## License:

This work is licensed under a Creative Commons Attribution 2.0 South Africa License.
